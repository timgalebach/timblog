(defproject timblog "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.stuartsierra/component "0.3.1"]
                 [compojure "1.4.0"]
                 [org.immutant/web "2.1.2"]
                 [prismatic/schema "1.0.4"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-devel "1.3.2"]
                 [danlentz/clj-uuid "0.1.6"]
                 [slugger "1.0.1"]
                 [clj-time "0.11.0"]
                 [clj-aws-s3 "0.3.10"]]
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.7"]
                                  [org.clojure/java.classpath "0.2.2"]]
                   :source-paths ["dev"]}
             :uberjar {:source-paths ["uberjar"]
                       :main main
                       :aot [main]}}
  :plugins [[lein-environ "1.0.2"]])
