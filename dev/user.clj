(ns user
  "Tools for interactive development with the REPL. This file should
  not be included in a production build of the application."
  (:require
   [clojure.java.io :as io]
   [clojure.java.javadoc :refer [javadoc]]
   [clojure.pprint :refer [pprint]]
   [clojure.reflect :refer [reflect]]
   [clojure.repl :refer [apropos dir doc find-doc pst source]]
   [clojure.set :as set]
   [clojure.string :as string]
   [schema.core :as s]
   [compojure.core :refer :all]
   [compojure.route :as route]
   [clojure.tools.namespace.repl :refer [refresh refresh-all]]
   [com.stuartsierra.component :as component]
   [immutant.web :as web]
   [clj-uuid :as uuid]
   [clj-time.core :as t]
   [clj-time.coerce :as c]
   [aws.sdk.s3 :as s3]
   [slugger.core :as slug]
   [timblog :as app]
   [blog :as b]
   [models :as m]))

(s/set-fn-validation! true)

(def system
  "A Var containing an object representing the application under
  development."
  nil)

(defn inject-vars [])

(defn init
  "Creates and initializes the system under development in the Var
  #'system."
  []
  (alter-var-root #'system
                  (constantly (app/system "config.edn"))))

(defn start
  "Starts the system running, updates the Var #'system."
  []
  (alter-var-root #'system component/start)
  )

(defn stop
  "Stops the system if it is currently running, updates the Var
  #'system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

(defn go
  "Initializes and starts the system running."
  []
  (init)
  (start)
#_  (inject-vars)
  :ready)

(defn reset
  "Stops the system, reloads modified source files, and restarts it."
  []
  (stop)
  (refresh :after 'user/go))

(comment
  ;;code to test server
  (click/process-click! (f/mock-request-map db cc "DE"))
  (pprint (read-string (click/process-click! (f/mock-request-map db cc "DE"))))
  )
