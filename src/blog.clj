(ns blog
  (:require [compojure.core :refer :all]
            [aws.sdk.s3 :as s3]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [schema.core :as s]
            [slugger.core :refer [->slug]]
            [clojure.string :refer [join]]
            [clj-uuid :as uuid]
            [models :as m]))

(s/defn make-s3-key :- m/S3Key
  [{:keys [id timestamp]} :- m/Post]
  (let [[y m d] ((juxt t/year t/month t/day) (c/from-string timestamp))
        leading-zero (fn [i] (if (< i 10) (str "0" i) (str i)))]
    (join "-" (concat (map leading-zero [y m d]) [id]))))

(s/defn get-post [post-atom s3-creds slug]
  #_(s3/get-object s3-creds "timblog" ))

(defroutes blog-routes
  (GET "/post/:slug" [post-atom s3-creds slug] (str "slug is " slug)))

(s/defn new-post :- m/Post
  [title     :- s/Str
   content   :- s/Str]
  {:id (-> (uuid/v1) uuid/to-string)
   :title title
   :slug  (->slug title)
   :timestamp (c/to-string (t/now))
   :content content})

(s/defn add-post
  [post-map
   new-post :- m/Post]
  (assoc post-map (:slug new-post) new-post))

(s/defn persist-post!
  [s3-creds
   post :- m/Post]
  (s3/put-object s3-creds "timblog" (make-s3-key post) (pr-str post)))
