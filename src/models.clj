(ns models
  (:require [schema.core :as s]))

(def DateTime org.joda.time.DateTime)

(def UUID s/Str)
(def S3Key s/Str)

(def Post
  {:id    s/Str
   :title s/Str
   :slug s/Str
   :timestamp  s/Str
   :content s/Str})
