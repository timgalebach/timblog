(ns timblog
  (:require [com.stuartsierra.component :as component]
            [ring.middleware.params :refer [wrap-params]]
            [immutant.web :as web]
            [compojure.core :refer :all]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [clojure.java.io :refer [resource]]
            [aws.sdk.s3 :as s3]
            [schema.core :as s]
            [blog :as blog]))

(s/defn wrap-state
  [handler post-atom s3-creds]
  (fn [request]
    (handler (-> request
                 (assoc-in [:params :post-atom] post-atom)
                 (assoc-in [:params :s3-creds]  s3-creds)))))

(defn wrap-proxy
  "if proxy? is true, sets Ring request IP to X-Forwarded-For"
  [handler use-proxy?]
  (fn [request]
    (if use-proxy?
      (handler (assoc request
                      :remote-addr (get-in request [:headers "x-forwarded-for"])))
      (handler request))))

(defn wrap-params-keywords
  "turns all map keys in the :params key into keywords (not strings)"
  [handler]
  (fn [request]
    (handler (assoc request :params
                    (zipmap (map keyword (keys (:params request)))
                            (vals (:params request)))))))


(defrecord PostIndex [s3-creds store-key post-atom]
  component/Lifecycle
  (start [this]
    (assoc this :post-atom (atom (-> (s3/get-object s3-creds "timblog" store-key)
                                     :content slurp read-string))))
  (stop [this]
    {}))


(defrecord Handler [app db use-proxy? post-index]
  component/Lifecycle
  (start [this]
    (assoc this :app (-> app
                         (wrap-state (:post-atom post-index) (:s3-creds post-index))
                         (wrap-proxy use-proxy?)
                         wrap-cookies
                         wrap-params-keywords
                         wrap-params)))
  (stop [this]
    (assoc this :db nil :app nil :use-proxy? nil)))

(defrecord Server [port path handler]
  component/Lifecycle
  (start [this]
    (println "Starting server on port:" port)
    (assoc this :params (web/run (:app handler) :port port)))
  (stop [this]
    (web/stop :port port)
    (assoc this :port nil :path nil :handler nil)))

(defn system [config-file]
  (let [port 3004
        use-proxy? false
        config (-> config-file slurp read-string)
        {:keys [post-store-key env-file]} config
        env    (-> env-file slurp read-string)]
    (component/system-map
     :s3-creds   (:aws env)
     :post-index (component/using
                  (map->PostIndex {:store-key post-store-key})
                  [:s3-creds])
     :handler   (component/using
                 (map->Handler {:app blog/blog-routes :use-proxy? use-proxy?})
                 [:post-index])
     :server    (component/using
                 (map->Server {:port port})
                 [:handler]))))
